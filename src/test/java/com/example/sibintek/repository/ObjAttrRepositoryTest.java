package com.example.sibintek.repository;

import com.example.sibintek.common.Attributes;
import com.example.sibintek.common.JsonObject;
import com.example.sibintek.common.ObjectAttributes;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
class ObjAttrRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ObjAttrRepository repository;

    @Test
    void getByAttributesAndObjects() {
        JsonObject object = new JsonObject();
        Attributes attributes = new Attributes();
        object.setName("one");
        attributes.setName("attr");
        ObjectAttributes objectAttributes = new ObjectAttributes();
        objectAttributes.setAttributes(attributes);
        objectAttributes.setObject(object);
        entityManager.persist(object);
        entityManager.flush();
        entityManager.persist(attributes);
        entityManager.flush();
        entityManager.persist(objectAttributes);
        entityManager.flush();
        ObjectAttributes found = repository.getByAttributesAndObjects(attributes, object);
        assertThat(found.getObject().getName()).isEqualTo(object.getName());
    }

    @Test
    void getByAttributesAndValue() {
        JsonObject object = new JsonObject();
        Attributes attributes = new Attributes();
        object.setName("one");
        attributes.setName("attr");
        ObjectAttributes objectAttributes = new ObjectAttributes();
        objectAttributes.setAttributes(attributes);
        objectAttributes.setObject(object);
        objectAttributes.setValue("1");
        JsonObject object1 = new JsonObject();
        object1.setName("two");
        ObjectAttributes objectAttributes1 = new ObjectAttributes();
        objectAttributes1.setAttributes(attributes);
        objectAttributes1.setObject(object1);
        objectAttributes1.setValue("1");
        entityManager.persist(object);
        entityManager.flush();
        entityManager.persist(attributes);
        entityManager.flush();
        entityManager.persist(objectAttributes);
        entityManager.flush();
        entityManager.persist(object1);
        entityManager.flush();
        entityManager.persist(objectAttributes1);
        entityManager.flush();
        List<ObjectAttributes> expected = new ArrayList<>(2);
        expected.add(objectAttributes);
        expected.add(objectAttributes1);
        List<ObjectAttributes> actual = repository.getByAttributesAndValue(attributes, "1");
        assertThat(actual).isEqualTo(expected);
    }
}