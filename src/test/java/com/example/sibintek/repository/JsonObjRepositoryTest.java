package com.example.sibintek.repository;

import com.example.sibintek.common.JsonObject;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
class JsonObjRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private JsonObjRepository repository;

    @Test
    void getByName() {
        JsonObject object = new JsonObject();
        object.setName("one");
        entityManager.persist(object);
        entityManager.flush();
        JsonObject actual = repository.getByName("one");
        assertThat(actual).isEqualTo(object);
    }

    @Test
    void getById() {
        JsonObject object = new JsonObject();
        object.setName("one");
        entityManager.persist(object);
        entityManager.flush();
        JsonObject actual = repository.getById(1);
        assertThat(actual).isEqualTo(object);
    }
}