package com.example.sibintek.repository;

import com.example.sibintek.common.Attributes;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
class AttributesRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private AttributesRepository repository;

    @Test
    void getByName() {
        Attributes expected = new Attributes();
        expected.setName("aaa");
        entityManager.persist(expected);
        entityManager.flush();
        Attributes actual = repository.getByName("aaa");
        assertThat(actual).isEqualTo(expected);
    }
}