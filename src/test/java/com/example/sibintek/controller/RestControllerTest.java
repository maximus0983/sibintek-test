package com.example.sibintek.controller;

import com.example.sibintek.common.JsonObject;
import com.example.sibintek.service.Service;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RestController.class)
class RestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    Service service;

    @Test
    void saveEntity() throws Exception {
        mockMvc.perform(post("/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"val\":\"12\"}"))
                .andExpect(status().isCreated());
    }

    @Test
    void getObject() throws Exception {
        Map<String, String> expected = new LinkedHashMap<>();
        expected.put("val", "12");
        when(service.getMapById(anyInt())).thenReturn(expected);
        mockMvc.perform(get("/get/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.val").value("12"));
    }

    @Test
    void updateObject() throws Exception {
        JsonObject object = new JsonObject();
        when(service.updateObject(1, "{\"val\":\"12\"}")).thenReturn(object);
        mockMvc.perform(put("/update/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"val\":\"12\"}"))
                .andExpect(status().isOk());
    }

    @Test
    void getByAttributeAndValue() throws Exception {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("val", "12");
        List<Map<String, String>> expected = new ArrayList<>();
        expected.add(map);
        when(service.getByAttributeAndValue(anyString(), anyString())).thenReturn(expected);
        mockMvc.perform(get("/getby/val/12"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$[*].val").value("12"));
    }
}