package com.example.sibintek.service;

import com.example.sibintek.common.Attributes;
import com.example.sibintek.common.JsonObject;
import com.example.sibintek.common.ObjectAttributes;
import com.example.sibintek.repository.AttributesRepository;
import com.example.sibintek.repository.JsonObjRepository;
import com.example.sibintek.repository.ObjAttrRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class ServiceTest {
    @Autowired
    private Service service;
    @MockBean
    private AttributesRepository attributesRepository;
    @MockBean
    private ObjAttrRepository objAttrRepository;
    @MockBean
    private JsonObjRepository jsonObjRepository;

    @Test
    void getMapById() {
        JsonObject object = new JsonObject();
        object.setName("obj");
        Attributes attr = new Attributes();
        attr.setName("val");
        Attributes attr1 = new Attributes();
        attr1.setName("val2");
        ObjectAttributes objAttr = new ObjectAttributes();
        objAttr.setValue("1");
        objAttr.setAttributes(attr);
        ObjectAttributes objAttr1 = new ObjectAttributes();
        objAttr1.setValue("2");
        objAttr1.setAttributes(attr1);
        objAttr.setObject(object);
        objAttr1.setObject(object);
        List<ObjectAttributes> attributesList = new ArrayList<>();
        attributesList.add(objAttr);
        attributesList.add(objAttr1);
        object.setAttributes(attributesList);
        when(jsonObjRepository.getById(anyInt())).thenReturn(object);
        Map<String, String> expected = new LinkedHashMap<>();
        expected.put("val", "1");
        expected.put("val2", "2");
        assertThat(service.getMapById(1)).isEqualTo(expected);
    }

    @Test
    void updateObject() throws JsonProcessingException {
        JsonObject object = new JsonObject();
        object.setName("obj");
        when(jsonObjRepository.getById(anyInt())).thenReturn(object);
        Attributes attr = new Attributes();
        attr.setName("val");
        when(attributesRepository.getByName(anyString())).thenReturn(attr);
        ObjectAttributes objAttr = new ObjectAttributes();
        objAttr.setObject(object);
        objAttr.setAttributes(attr);
        objAttr.setValue("lavalue");
        object.setAttributes(Arrays.asList(objAttr));
        when(objAttrRepository.getByAttributesAndObjects(attr, object)).thenReturn(objAttr);
        when(objAttrRepository.save(objAttr)).thenReturn(objAttr);
        String json = "{\"val\":\"12\"}";
        assertThat(service.updateObject(1, json).getAttributes().get(0).getValue()).isEqualTo("12");
    }

    @Test
    void getByAttributeAndValue() {
        JsonObject object = new JsonObject();
        object.setName("obj");
        object.setId(1);
        Attributes attr = new Attributes();
        attr.setName("val");
        attr.setId(1);
        when(attributesRepository.getByName(anyString())).thenReturn(attr);
        ObjectAttributes objAttr = new ObjectAttributes();
        List<ObjectAttributes> objAttrList = new ArrayList<>();
        objAttr.setValue("12");
        objAttr.setAttributes(attr);
        objAttr.setObject(object);
        objAttrList.add(objAttr);
        object.setAttributes(objAttrList);
        when(objAttrRepository.getByAttributesAndValue(any(), any())).thenReturn(objAttrList);
        Map<String, String> resultGetById = new LinkedHashMap<>();
        resultGetById.put("val", "12");
        List<Map<String, String>> expected = new ArrayList<>();
        expected.add(resultGetById);
        when(jsonObjRepository.getById(anyInt())).thenReturn(object);
        assertThat(service.getByAttributeAndValue("val", "12")).isEqualTo(expected);
    }
}