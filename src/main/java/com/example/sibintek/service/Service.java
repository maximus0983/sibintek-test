package com.example.sibintek.service;

import com.example.sibintek.common.Attributes;
import com.example.sibintek.common.JsonObject;
import com.example.sibintek.common.ObjectAttributes;
import com.example.sibintek.repository.AttributesRepository;
import com.example.sibintek.repository.JsonObjRepository;
import com.example.sibintek.repository.ObjAttrRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Service
public class Service {
    @Autowired
    private AttributesRepository attributesRepository;
    @Autowired
    private JsonObjRepository jsonObjRepository;
    @Autowired
    private ObjAttrRepository objAttrRepository;

    public void save(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonObject object = new JsonObject();
        object.setName(object.toString());
        jsonObjRepository.save(object);
        JsonObject obj = jsonObjRepository.getByName(object.toString());
        Map<String, Object> jsonMap = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
        });
        for (Map.Entry me : jsonMap.entrySet()) {
            String key = (String) me.getKey();
            Attributes attr = attributesRepository.getByName(key);
            if (attr == null) {
                Attributes tempAttr = new Attributes();
                tempAttr.setName(key);
                attributesRepository.save(tempAttr);
                attr = attributesRepository.getByName(tempAttr.getName());
            }
            ObjectAttributes objectAttributes = new ObjectAttributes();
            objectAttributes.setObject(obj);
            objectAttributes.setAttributes(attr);
            objectAttributes.setValue(String.valueOf(me.getValue()));
            objAttrRepository.save(objectAttributes);
        }
    }

    public Map<String, String> getMapById(int id) {
        JsonObject object = jsonObjRepository.getById(id);
        if (object == null) return null;
        List<ObjectAttributes> objectAttributes = object.getAttributes();
        Map<String, String> result = new LinkedHashMap<>();
        for (ObjectAttributes temp : objectAttributes) {
            String key = temp.getAttributes().getName();
            String value = temp.getValue();
            result.put(key, value);
        }
        return result;
    }

    public JsonObject updateObject(int id, String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonMap = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
        });
        JsonObject object = jsonObjRepository.getById(id);
        if (object == null) return null;
        for (Map.Entry me : jsonMap.entrySet()) {
            Attributes attribute = attributesRepository.getByName(me.getKey().toString());
            ObjectAttributes objectAttributes = objAttrRepository.getByAttributesAndObjects(attribute, object);
            objectAttributes.setValue(me.getValue().toString());
            objAttrRepository.save(objectAttributes);
        }
        return object;
    }

    public List<Map<String, String>> getByAttributeAndValue(String attribute, String value) {
        Attributes attributes = attributesRepository.getByName(attribute);
        if (attributes == null) return null;
        List<ObjectAttributes> objectAttributes = objAttrRepository.getByAttributesAndValue(attributes, value);
        List<JsonObject> objects = new ArrayList<>();
        for (ObjectAttributes obj : objectAttributes) {
            objects.add(obj.getObject());
        }
        List<Map<String, String>> result = new ArrayList<>();
        for (JsonObject obj : objects) {
            result.add(getMapById(obj.getId()));
        }
        return result;
    }
}