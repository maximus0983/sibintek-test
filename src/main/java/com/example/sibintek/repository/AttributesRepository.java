package com.example.sibintek.repository;

import com.example.sibintek.common.Attributes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttributesRepository extends JpaRepository<Attributes, Integer> {
    Attributes getByName(String name);
}