package com.example.sibintek.repository;

import com.example.sibintek.common.Attributes;
import com.example.sibintek.common.JsonObject;
import com.example.sibintek.common.ObjectAttributes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ObjAttrRepository extends JpaRepository<ObjectAttributes, Integer> {

    ObjectAttributes getByAttributesAndObjects(Attributes attributes, JsonObject object);

    List<ObjectAttributes> getByAttributesAndValue(Attributes attributes, String value);
}