package com.example.sibintek.repository;

import com.example.sibintek.common.JsonObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JsonObjRepository extends JpaRepository<JsonObject, Integer> {
    JsonObject getByName(String name);

    JsonObject getById(int id);
}
