package com.example.sibintek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SibintekApplication {

    public static void main(String[] args) {
        SpringApplication.run(SibintekApplication.class, args);
    }

}
