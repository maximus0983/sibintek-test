package com.example.sibintek.controller;

import com.example.sibintek.common.JsonObject;
import com.example.sibintek.service.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Autowired
    private Service service;

    @PostMapping("/save")
    public ResponseEntity saveEntity(@RequestBody String json) throws JsonProcessingException {
        service.save(json);
        return new ResponseEntity("Successfully created ", new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Map<String, String>> getObject(@PathVariable("id") int id) {
        Map<String, String> result = service.getMapById(id);
        if (result == null) {
            return new ResponseEntity("Object not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        } else return new ResponseEntity(result, new HttpHeaders(), HttpStatus.OK);

    }


    @PutMapping("/update/{id}")
    public ResponseEntity updateObject(@PathVariable("id") int id, @RequestBody String json) throws JsonProcessingException {
        JsonObject object = service.updateObject(id, json);
        if (object == null) {
            return new ResponseEntity("Product not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity("Successfully updated ", new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("getby/{attribute}/{value}")
    public ResponseEntity<List<Map<String, String>>> getByAttributeAndValue(@PathVariable("attribute") String attribute,
                                                                            @PathVariable("value") String value) {
        List<Map<String, String>> attributeAndValue = service.getByAttributeAndValue(attribute, value);
        if (attributeAndValue == null) {
            return new ResponseEntity("Objects not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(attributeAndValue, new HttpHeaders(), HttpStatus.OK);
    }
}