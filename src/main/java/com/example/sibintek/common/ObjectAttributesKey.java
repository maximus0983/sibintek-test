package com.example.sibintek.common;

import java.io.Serializable;
import java.util.Objects;

public class ObjectAttributesKey implements Serializable {
    private Integer objects;
    private Integer attributes;

    public ObjectAttributesKey(Integer objects, Integer attributes) {
        this.objects = objects;
        this.attributes = attributes;
    }

    public ObjectAttributesKey() {
    }

    public Integer getObjects() {
        return objects;
    }

    public void setObjects(Integer objects) {
        this.objects = objects;
    }

    public Integer getAttributes() {
        return attributes;
    }

    public void setAttributes(Integer attributes) {
        this.attributes = attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectAttributesKey that = (ObjectAttributesKey) o;
        return Objects.equals(objects, that.objects) &&
                Objects.equals(attributes, that.attributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objects, attributes);
    }
}