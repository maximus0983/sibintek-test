package com.example.sibintek.common;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@XmlRootElement
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "attributes")
@Table(name = "objectattributes")
@IdClass(ObjectAttributesKey.class)
public class ObjectAttributes implements Serializable {
    private String value;
    @Id
    @MapsId("json_id")
    @ManyToOne
    @JoinColumn(name = "json_Id")
    private JsonObject objects;
    @Id
    @ManyToOne
    @MapsId("attribute_id")
    @JoinColumn(name = "attribute_Id")
    private Attributes attributes;

    public ObjectAttributes() {
    }

    public ObjectAttributes(JsonObject object, Attributes attributes) {
        this.objects = object;
        this.attributes = attributes;
    }

    public JsonObject getObject() {
        return objects;
    }

    public void setObject(JsonObject object) {
        this.objects = object;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectAttributes that = (ObjectAttributes) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(objects, that.objects) &&
                Objects.equals(attributes, that.attributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, objects, attributes);
    }
}